package com.dropbox.api.samples.chooser_start;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.api.samples.MainActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class Finished extends Activity {

    Boolean pass = false;
    public static int result  = 0;
    public static String[] incorrect;
    String[][] data;
    public static String exam = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finished);

        Intent intent = getIntent();
        if(Integer.parseInt(intent.getStringExtra("total")) > 0) {
            int correct = Integer.parseInt(intent.getStringExtra("correct"));
            int total = Integer.parseInt(intent.getStringExtra("total"));
            exam = intent.getStringExtra("exam");
            String time = intent.getStringExtra("time");
            incorrect = intent.getStringArrayExtra("incorrect");
            determineMark(correct,total);


            renderMark(time);


            saveStatistics();
        }

        Button reset = (Button) findViewById(R.id.btnRestart);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restart();
            }
        });

        Button results = (Button) findViewById(R.id.btnResults);
        results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                results();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_finished, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void results() {
        String output = "";
        for(int j =0;j<incorrect.length;j++){
            output = output + "-" + incorrect[j] + "\n";

        }
        final TextView txtIncorrect = (TextView)findViewById(R.id.txtIncorrect);
        txtIncorrect.setText(output);
        final TextView txtIncorrectHeader = (TextView)findViewById(R.id.txtIncorrectHeader);
        txtIncorrectHeader.setText("Incorrect Questions");
    }



    public void renderMark(String time) {
        final TextView txtResult = (TextView)findViewById(R.id.txtResult);
        txtResult.setText(Double.toString(Math.floor(result)) + "%\n" + time);



    }
    public void determineMark(int correct, int total) {
        result = (correct * 100) / total;
        if(result >= 50) {
            pass = true;
            Toast.makeText(this, "You have passed!", Toast.LENGTH_LONG).show();
        } else {
            pass = false;
            Toast.makeText(this, "You failed!", Toast.LENGTH_LONG).show();
        }
    }



    public void restart() {
        Intent in = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(in);
    }




    public void saveStatistics() {

        MainActivity objMain = new MainActivity();
        String currentData = objMain.readStatistics();
        String filename = "preStudy.txt";

        FileOutputStream outputStream;
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        String finalLine = currentData +  exam + "#" + currentDateTimeString+ "#" + Integer.toString(result) + "\n" ;
        try {
            getApplicationContext();
            outputStream = openFileOutput(filename, MODE_APPEND);
            outputStream.write(finalLine.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
