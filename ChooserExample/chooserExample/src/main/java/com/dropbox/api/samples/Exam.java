package com.dropbox.api.samples;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.dropbox.api.samples.chooser_start.Finished;
import com.dropbox.api.samples.chooser_start.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class Exam   extends Activity {
    Map<Integer, String[]> data = new HashMap<Integer, String[]>();

    private static String pipe = "|";
    RadioGroup rgOpinion;
    //URL to get JSON Array
    String[] correctAnswer;
    List<String> incorrectQuestions = new ArrayList<String>();
    //JSON Node Names
    private static Boolean checkbox1_correct = false;
    private static Boolean checkbox2_correct = false;
    private static Boolean checkbox3_correct = false;
    private static Boolean checkbox4_correct = false;
    private static int max_questions = 0;
    private static int question_nr = 0;
    private static int correct_questions = 0;
    private static int incorrect_questions = 0;
    private static String correct = "";
    private static String question = "";
    private static String exam = "";
    public static URL url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        String docUrl = intent.getStringExtra("url");
        exam = intent.getStringExtra("exam");
        exam = exam.replace(".csv","");
        try {
            url = new URL(docUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        init();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_exam);

        getContent();
        Button generate = (Button) findViewById(R.id.btnQuestion);
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateQuestion(true);
            }
        });

        Button restart = (Button) findViewById(R.id.btnRestart);
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(in);
            }
        });




    }
    public void init() {
        max_questions = 0;
        question_nr = 0;
        correct_questions = 0;
        incorrect_questions = 0;



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getContent() {



        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = "\",\"";

        try {

            br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
            int counter = 0;
            Boolean invalidLine = false;
            while ((line = br.readLine()) != null) {
                int NrofQuotes = numberOf("\"",line);
                int NrofHashes = numberOfHashes("#",line);
                //If line has 6 double quotes and at least 1 hash
               if(NrofQuotes == 6 && NrofHashes > 0 && NrofHashes < 4) {
                   line = line.replaceAll("^\"|\"$", "");
                   String[] currentLine = line.split(cvsSplitBy);
                   data.put(counter, currentLine);
                   counter++;
               } else {
                   invalidLine = true;
               }

            }

            if(invalidLine) {
                Toast.makeText(this, "Invalid CSV format!", Toast.LENGTH_LONG).show();
                Intent in = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(in);
            } else {
                max_questions = data.size();
                generateQuestion(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }




    }
    public static int numberOf(String target, String content)
    {
        return (content.split(target).length);
    }

    public static int numberOfHashes(String target, String content)
    {
        return (content.split(target).length -1);
    }

    public void reset(TextView txtQuestion) {
        txtQuestion.setText("");
    }
    public void generateQuestion(Boolean withAnswer) {


        if(withAnswer == true) {

            final CheckBox rdn1 = (CheckBox)findViewById(R.id.checkBox1);
            final CheckBox rdn2 = (CheckBox)findViewById(R.id.checkBox2);
            final CheckBox rdn3 = (CheckBox)findViewById(R.id.checkBox3);
            final CheckBox rdn4 = (CheckBox)findViewById(R.id.checkBox4);
            if(rdn1.isChecked() || rdn2.isChecked() || rdn3.isChecked() || rdn4.isChecked()) {
                checkAnswer();
            } else {
                Toast.makeText(this, "Select at least one option!", Toast.LENGTH_LONG).show();
            }

        } else {
            startChronometer();
        }
        resetChecks();

        final TextView txtQuestion = (TextView)findViewById(R.id.txtQuestion);
        reset(txtQuestion);


        try {

            String[] current = data.get(question_nr);
            correct = current[2];
            question = current[0];
            txtQuestion.setText(question);


            String answers = current[1];
            renderAnswers(answers);




            final ProgressBar progress = (ProgressBar)findViewById(R.id.progressBar);
            progress.setMax(max_questions);
            progress.setProgress(question_nr);




        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void resetChecks() {
        final CheckBox rdn = (CheckBox)findViewById(R.id.checkBox1);
        rdn.setChecked(false);
        rdn.setText("");
        rdn.setVisibility(View.INVISIBLE);

        final CheckBox rdn1 = (CheckBox)findViewById(R.id.checkBox2);
        rdn1.setChecked(false);
        rdn1.setText("");
        rdn1.setVisibility(View.INVISIBLE);

        final CheckBox rdn2 = (CheckBox)findViewById(R.id.checkBox3);
        rdn2.setChecked(false);
        rdn2.setText("");
        rdn2.setVisibility(View.INVISIBLE);

        final CheckBox rdn3 = (CheckBox)findViewById(R.id.checkBox4);
        rdn3.setChecked(false);
        rdn3.setText("");
        rdn3.setVisibility(View.INVISIBLE);

        checkbox1_correct = false;
        checkbox2_correct = false;
        checkbox3_correct = false;
        checkbox4_correct = false;

    }

    public static boolean in_array(String[] haystack, String needle) {
        for(int i=0;i<haystack.length;i++) {
            if(haystack[i].equals(needle)) {
                return true;
            }
        }
        return false;
    }


    public static String[] shuffleArray(String[] array) {
        List<String> list = new ArrayList<String>();
        for (String i : array) {
            list.add(i);
        }

        Collections.shuffle(list);

        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }


    public void renderAnswers(String answers) {

        final RadioGroup rdnGroup = (RadioGroup)findViewById(R.id.rgOpinion);
        rdnGroup.setVisibility(View.VISIBLE);
        correctAnswer = correct.split("#");
        String[] currentAnswer = answers.split("#");
        currentAnswer = shuffleArray(currentAnswer);
        try {
            for(int i = 0 ; i < currentAnswer.length; i++){

                if(i == 0) {
                    String answer = currentAnswer[0];
                    final CheckBox rdn = (CheckBox)findViewById(R.id.checkBox1);
                    rdn.setVisibility(View.VISIBLE);
                    rdn.setText(answer);
                    if(in_array(correctAnswer,answer)) {
                        checkbox1_correct = true;
                    }

                } else if(i == 1) {
                    String answer = currentAnswer[1];
                    final CheckBox rdn = (CheckBox)findViewById(R.id.checkBox2);
                    rdn.setVisibility(View.VISIBLE);
                    rdn.setText(answer);
                    if(in_array(correctAnswer, answer)) {
                        checkbox2_correct = true;
                    }
                }
                else if(i == 2) {
                    String answer = currentAnswer[2];
                    final CheckBox rdn = (CheckBox)findViewById(R.id.checkBox3);
                    rdn.setVisibility(View.VISIBLE);
                    rdn.setText(answer);
                    if(in_array(correctAnswer, answer)) {
                        checkbox3_correct = true;
                    }
                }
                else if(i == 3) {
                    String answer = currentAnswer[3];
                    final CheckBox rdn = (CheckBox)findViewById(R.id.checkBox4);
                    rdn.setVisibility(View.VISIBLE);
                    rdn.setText(answer);
                    if(in_array(correctAnswer, answer)) {
                        checkbox4_correct = true;
                    }
                }

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void startChronometer() {
        ((Chronometer) findViewById(R.id.Chronometer1)).start();
    }

    public void stopChronometer() {
        ((Chronometer) findViewById(R.id.Chronometer1)).stop();
    }


    public void testComplete() {
        stopChronometer();
        Intent in = new Intent(getApplicationContext(),Finished.class);

//
//        /*ArrayList to Array Conversion */
        String array[] = new String[incorrectQuestions.size()];
        for(int j =0;j<incorrectQuestions.size();j++){
            array[j] = incorrectQuestions.get(j);
        }

        Chronometer timer = ((Chronometer) findViewById(R.id.Chronometer1));
        long timeElapsed = SystemClock.elapsedRealtime() - timer.getBase();
        int minutes = (int) ((timeElapsed / (1000*60)) % 60);
        int seconds = (int) (timeElapsed / 1000) % 60 ;
        int hours   = (int) ((timeElapsed / (1000*60*60)) % 24);
        String timeString = String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
        in.putExtra("time", timeString);
        in.putExtra("exam", exam);
        in.putExtra("correct", Integer.toString(correct_questions));
       in.putExtra("incorrect", array);
       in.putExtra("total", Integer.toString(max_questions));
        try {
            startActivity(in);
        } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

    }

    public void checkAnswer() {

        int NrofGoodAnswers = correctAnswer.length;
        Boolean fail = false;
        int success = 0;
        final CheckBox rdn1 = (CheckBox)findViewById(R.id.checkBox1);
        if(rdn1.isChecked()) {
            if (checkbox1_correct) {
                success++;
            } else {
                fail = true;
            }
        }

        final CheckBox rdn2 = (CheckBox)findViewById(R.id.checkBox2);
        if(rdn2.isChecked()) {
            if (checkbox2_correct) {
                success++;
            } else {
                fail = true;
            }
        }

        final CheckBox rdn3 = (CheckBox)findViewById(R.id.checkBox3);
        if(rdn3.isChecked()) {
            if (checkbox3_correct) {
                success++;
            } else {
                fail = true;
            }
        }

        final CheckBox rdn4 = (CheckBox)findViewById(R.id.checkBox4);
        if(rdn4.isChecked()) {
            if (checkbox4_correct) {
                success++;
            } else {
                fail = true;
            }
        }


        if(success == NrofGoodAnswers && !fail) {
//            Toast.makeText(this, "CORRECT",
//                    Toast.LENGTH_LONG).show();
            correct_questions++;
        } else {
//            Toast.makeText(this, "WRONG!",
//                    Toast.LENGTH_LONG).show();
            incorrect_questions++;
            incorrectQuestions.add(question);
        }

        question_nr++;
        if(question_nr == max_questions) {
            testComplete();
        }


    }
}
