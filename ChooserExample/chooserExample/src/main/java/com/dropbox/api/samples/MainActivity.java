package com.dropbox.api.samples;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.dropbox.api.samples.chooser_start.Information;
import com.dropbox.api.samples.chooser_start.R;
import com.dropbox.chooser.android.DbxChooser;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    static final String APP_KEY ="wqrdqmuz931ugpj";
    static final int DBX_CHOOSER_REQUEST = 0;  // You can change this if needed

    private Button mChooserButton;
    private DbxChooser mChooser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showStatistics();
        mChooser = new DbxChooser(APP_KEY);

        mChooserButton = (Button) findViewById(R.id.btnStart);
        mChooserButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DbxChooser.ResultType resultType;
                resultType = DbxChooser.ResultType.DIRECT_LINK;
                
                mChooser.forResultType(resultType)
                        .launch(MainActivity.this, DBX_CHOOSER_REQUEST);
            }
        });

        Button help = (Button) findViewById(R.id.btnHelp);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),Information.class);
                startActivity(in);
            }
        });



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DBX_CHOOSER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                DbxChooser.Result result = new DbxChooser.Result(data);
                String fileName = result.getName();
                if(fileName.contains(".csv")) {
                    Intent in = new Intent(getApplicationContext(),Exam.class);
                    in.putExtra("url", result.getLink().toString());
                    in.putExtra("exam", result.getName().toString());
                    startActivity(in);
                } else {
                    Toast.makeText(this, "File selected is not CSV format", Toast.LENGTH_LONG).show();
                }

            } else {
                // Failed or was cancelled by the user.
                Toast.makeText(this, "Dropbox Connection Failed", Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    public void showStatistics() {

            TableLayout prices = (TableLayout) findViewById(R.id.TableLayout1);
            prices.setStretchAllColumns(true);
            prices.bringToFront();

            String buffer = readStatistics();
            int line = 0;
            int maxLines = 11;
            if (buffer.length() > 0) {
                line++;
                String[] lines = buffer.split("\n");


                //sort by latest first
                List<String> list = Arrays.asList(lines);
                Collections.reverse(list);
                lines = (String[]) list.toArray();


                for (int j = 0; j < lines.length; j++) {
                    if (j < maxLines) {
                        String currentLine = lines[j];

                            String[] current = currentLine.split("#");
                            TableRow tr = new TableRow(this);
                            TextView c1 = new TextView(this);
                            String examName = current[0];
                            if (examName.length() > 15)
                                examName = examName.substring(0, 12) + "...";
                            c1.setText(examName);
                            c1.setTextSize(17);
                            c1.setHeight(70);
                            tr.addView(c1);


                            TextView c2 = new TextView(this);
                            c2.setText(current[1]);
                            c2.setTextSize(17);
                            c2.setHeight(70);
                            c2.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                            tr.addView(c2);


                            TextView c3 = new TextView(this);
                            c3.setText(current[2] + "%");
                            c3.setTextSize(17);
                            c3.setHeight(70);
                            tr.addView(c3);
                            prices.addView(tr);


                    }
                }


                final TextView txtPrevious = (TextView) findViewById(R.id.txtPrevious2);
                txtPrevious.setVisibility(View.VISIBLE);
            }


    }

    public String readStatistics() {

        String dataLines = "";
        try {
            FileInputStream fin = openFileInput("preStudy.txt");

            int c;

            while ((c = fin.read()) != -1) {
                dataLines = dataLines + Character.toString((char) c);
            }
            fin.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataLines;
    }


}
